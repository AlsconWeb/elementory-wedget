<?php

/**
 * Created 11.07.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 */

class productList
{
  protected static $instance = null;

  public static function get_instance()
  {
    if (!isset(static::$instance)) {
      static::$instance = new static;
    }

    return  static::$instance;
  }
  protected function __construct()
  {
    require_once("widget/product_list_widget.php");
    add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets']);
    add_action('elementor/frontend/after_enqueue_styles', [$this, 'product_widget_style']);
  }

  public function register_widgets()
  {
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\productListWidget());
  }

  public function product_widget_style()
  {
    wp_enqueue_style('product_list_style', get_stylesheet_directory_uri() . '/elementory/widget/css/main.css', array(), '');
  }
}

add_action('init', 'init_elementor_product_list');

function init_elementor_product_list()
{
  productList::get_instance();
}
