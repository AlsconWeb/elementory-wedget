<?php

/**
 * Created 11.07.19
 * Version 1.0.1
 * Last update 12.07.19
 * Author: Alex L
 */

namespace Elementor;

class productListWidget extends Widget_Base
{
  // name widget
  public function get_name()
  {
    return 'product_list';
  }

  // title widget
  public function get_title()
  {
    return __('Product List', 'product_list');
  }

  // icon widget
  public function get_icon()
  {
    return 'fa fa-list-alt';
  }

  // category widget
  function get_categories()
  {
    return ['basic'];
  }

  // add controls widget
  protected function _register_controls()
  {
    $this->start_controls_section(
      'content_section',
      [
        'label' => __('Content', 'product_list'),
        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
      ]
    );

    // add repeater
    $repeater = new \Elementor\Repeater();

    $repeater->add_control(
      'image_product',
      [
        'label' => __('Choose Image', 'product_list'),
        'type' => \Elementor\Controls_Manager::MEDIA,
        'default' => [
          'url' => \Elementor\Utils::get_placeholder_image_src(),
        ],
      ]
    );

    // title
    $repeater->add_control(
      'product_title',
      [
        'label' => __('Product Title', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'placeholder' => __('Enter title here', 'product_list'),
      ]
    );

    // brand
    $repeater->add_control(
      'product_brand',
      [
        'label' => __('Product Brand', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'placeholder' => __('Enter Brand here', 'product_list'),
      ]
    );
    // price From
    $repeater->add_control(
      'price_from',
      [
        'label' => __('Price From', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'placeholder' => __('Enter price from', 'product_list'),
      ]
    );

    // price to
    $repeater->add_control(
      'price_to',
      [
        'label' => __('Price to', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'placeholder' => __('Enter price to', 'product_list'),
      ]
    );

    // single price
    $repeater->add_control(
      'price',
      [
        'label' => __('Single price', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'placeholder' => __('Enter single price', 'product_list'),
      ]
    );

    // Button text
    $repeater->add_control(
      'button_text',
      [
        'label' => __('Button text', 'product_list'),
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => __('Pre order', 'product_list'),
        'placeholder' => __('Enter Button text', 'product_list'),
      ]
    );

    // URL button 
    $repeater->add_control(
      'btn_link',
      [
        'label' => __('Link', 'product_list'),
        'type' => \Elementor\Controls_Manager::URL,
        'placeholder' => __('https://your-link.com', 'product_list'),
        'show_external' => true,
        'default' => [
          'url' => '',
          'is_external' => false,
          'nofollow' => true,
        ],
      ]
    );

    // add controls in repeater
    $this->add_control(
      'list',
      [
        'label' => __('Repeater Product Item', 'product_list'),
        'type' => \Elementor\Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'prevent_empty' => true,
        'default' => [
          [
            'image_product' => __('Image', 'product_list'),
          ],
          ['product_title'],
          ['product_brand'],
          ['price_from'],
          ['price_to'],
          ['price'],
          ['button_text'],
          ['btn_link']
        ],
        'title_field' => 'Product',
      ]
    );




    $this->end_controls_section();
  }

  protected function render()
  {
    //Get data from item
    $settings = $this->get_settings_for_display();

    require_once('product_lust_widget_template.php');
  }
}
