<?php

/**
 * Created 11.07.19
 * Version 1.0.1
 * Last update 12.07.19
 * Author: Alex L
 */
?>
<div class="block widget block-products-list grid">
  <div class="block-content">
    <div class="products-grid grid">
      <ol class="product-items widget-product-grid">
        <?php foreach ($settings['list'] as $val) : ?>
          <li class="product-item">
            <div class="product-item-info">
              <a href="<?php echo $val['btn_link']['url']; ?>" class="product-item-photo"><span class="product-image-container" style="width:280px;"> <span class="product-image-wrapper" style="padding-bottom: 100%;"><?php if ($val['image_product']['url']) : ?><img class="product-image-photo" src="<?php echo $val['image_product']['url']; ?>" width="280" height="280" alt="<?php echo $val['product_title']; ?>"><?php else : ?><img class="product-image-photo" src="https://via.placeholder.com/280" width="280" height="280" alt="Placeholder"><?php endif; ?></span></span></a>

              <div class="product-item-details">
                <strong class="product-item-name">
                  <a title="<?php echo $val['product_title']; ?>" href="<?php echo $val['btn_link']['url']; ?>" class="product-item-link"><?php echo $val['product_title']; ?></a>
                </strong>
              </div>

              <div class="category-aec-artist" style="height: 25px;">
                <?php echo $val['product_brand']; ?>
              </div>

              <div class="price-box price-final_price" data-role="priceBox" data-product-id="13642" style="height: 44px;">
                <?php if ($val['price_from']) : ?>
                  <p class="price-from"><span class="price-container price-final_price tax weee"><span class="price-label">From</span><span id="old-price-13642-widget-product-grid" data-price-amount="70" data-price-type="minPrice" class="price-wrapper "> <span class="price"><?php echo $val['price_from']; ?></span></span></span>
                  </p>
                <?php endif; ?>
                <?php if ($val['price_to']) : ?>
                  <p class="price-to">
                    <span class="price-container price-final_price tax weee"><span class="price-label">To</span> <span id="old-price-13642-widget-product-grid" data-price-amount="85" data-price-type="maxPrice" class="price-wrapper "> <span class="price"><?php echo $val['price_to']; ?></span></span>
                    </span>
                  </p>
                <?php endif; ?>
                <?php if ($val['price']) : ?>
                  <span class="price-container price-final_price tax weee"><span id="old-price-13638-widget-product-grid" data-price-amount="40" data-price-type="finalPrice" class="price-wrapper "><span class="price"> <?php echo $val['price']; ?></span> </span></span>
                <?php endif; ?>
              </div>
              <?php if ($val['button_text']) : ?>
                <div class="product-item-actions">
                  <div class="actions-primary">
                    <a class="action tocart primary button_pre" href="<?php echo $val['btn_link']['url']; ?>"><span><?php echo $val['button_text']; ?></span>
                    </a>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ol>
    </div>
  </div>
</div>