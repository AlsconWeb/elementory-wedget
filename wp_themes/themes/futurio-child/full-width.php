<?php
/*
*
Template Name: Full-Width
*/
?>
<?php
get_header();

futurio_generate_header( true, true, true, true, true, true );

?>

<div id="primary" class="content-area">

  

        <?php

        // Start the loop.

        while ( have_posts() ) : the_post();

            // Include the page content template.

         // the_title( '<h1 class="entry-title">', '</h1>' );
          
          ?>

            <div class="entry-content">

                        <?php the_content(); ?>

            </div>
 <?php

        endwhile;

        ?>

   

</div><!-- .content-area -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
    
    $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 770) {
        $("#site-navigation").addClass("sticky");
    } else {
        $("#site-navigation").removeClass("sticky");
    }
});
</script>
<?php get_footer(); ?>