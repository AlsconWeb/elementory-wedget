<?php

/**
 * Function describe for Fiturio child
 * 
 * @package futurio-child
 */


add_action('wp_enqueue_scripts', 'futurio_child_enqueue_styles');
function futurio_child_enqueue_styles()
{

	wp_enqueue_style('futurio-stylesheet', get_template_directory_uri() . '/style.css', array('bootstrap'), '1.0.1');
	wp_enqueue_style('futurio-child-style', get_stylesheet_uri(), array('futurio-stylesheet'));
}


remove_action('astra_header', 'astra_header_markup');

function your_prefix_render_hfe_header()
{

	if (function_exists('hfe_render_header')) {
		hfe_render_header();
	}
}

add_action('astra_header', 'your_prefix_render_hfe_header');

function your_prefix_header_footer_elementor_support()
{
	add_theme_support('header-footer-elementor');
}

add_action('after_setup_theme', 'your_prefix_header_footer_elementor_support');


/**
 * Created 12.07.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 */

require_once "elementory/product_list.php";

/** End update */
